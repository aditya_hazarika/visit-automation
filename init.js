const User = require("./models/user-model");
const Template = require("./models/template-model");
const mongoose = require('mongoose');

mongoose.connect(
  "mongodb://application:pranav555@ds155164.mlab.com:55164/node-visit-automation",
  function() {
    console.log("mongodb connected");
    User.create({
      username: "superadmin",
      email: "superadmin@nimap.com",
      password: "123",
      company: "",
      isCompanyOwner: false,
      isSuperAdmin: true,
      isValid: true
    });

    Template.create({
      name: "default_template",
      subject: "Thanks for visiting",
      template: "<p>Hello [name],</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Thank you for visitin our stall.</p>\r\n\r\n<p>We are organized to serve&nbsp;[company_name].</p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>[username]</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n"
    }, (err, res) => {
        return
    });
  }
);
