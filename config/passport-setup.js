const passport = require('passport')
// const keys = require('./keys')
const User = require('../models/user-model');
const Group = require('../models/group-model');
const Template = require('../models/template-model')
const keys = require('../config/keys');
var GoogleStratergy = require('passport-google-oauth').OAuth2Strategy;
var refresh = require('passport-oauth2-refresh');
var mongoose = require('mongoose');

const LocalStratergy = require('passport-local').Strategy;

var default_template_id;
Template.findOne({ name: "default_template" }, (err, doc) => {
    if (err) throw err;
    default_template_id = doc._id;
});

passport.serializeUser(function (user, done) {
    done(null, user.id);
})

passport.deserializeUser(function (id, done) {
    User.findById(id).then(function (user) {
        done(null, user)
    })
})
var localOptions = {
    usernameField: 'email',
    session: true,

};


// var localStratergy = new LocalStratergy(localOptions, function (email, password, done) {
//     console.log("Other stratergy triggered");
//     User.findOne({ email: email }, (err, user) => {
//         if (err) return done(err);
//         if (!user) {
//             return done(null, false, { error: "Login failed." });
//         }

//         user.comparePassword(password, function (err, isMatch) {
//             if (err) {
//                 return done(err);
//             }
//             console.log("Password mathced:" + isMatch);
//             if (!isMatch) {
//                 return done(null, false, { error: "Login failed." });
//             }
//             return done(null, user);
//         })
//     })
// })

var googleStratergy = new GoogleStratergy({
    clientID: keys.google.client_id,
    clientSecret: keys.google.client_secret,
    prompt: 'true',
    accessType: 'offline',
    callbackURL: 'http://localhost:3000/login/google/callback',
}, function (accessToken, refreshToken, profile, done) {
    
    if (refreshToken) {
            User.findOne({email: profile.emails[0].value}, function(err, currentuser){
                if(currentuser){
                    User.findOneAndUpdate({ email: currentuser.email }, {isValid:true, gmail:{ accessToken: accessToken, refreshToken: refreshToken }}, (err, updated_user) => {
                        console.log(updated_user);
                        done(err, updated_user);
                    })
                }
                else{
                    var new_user_id = mongoose.Types.ObjectId();
                    var new_user = {
                        _id: new_user_id,
                        email: profile.emails[0].value,
                        role: "companyowner",
                        isValid:true,  
                        isCompanyOwner:true, 
                        group: [],
                        username:profile.displayName, 
                        gmail:{accessToken:accessToken, 
                        refreshToken:refreshToken}
                    }

                    // User.create({email: profile.emails[0].value, password:"any", role: "companyowner", isValid:true,  isCompanyOwner:true, username:profile.displayName, gmail:{accessToken:accessToken, refreshToken:refreshToken}},function(err,newuser){
                        Group.create({group_name: 'AllContacts',
                        template_id: default_template_id,
                        user: [new_user._id],
                        admin_id: new_user._id}, function(err, result){
                            new_user.group.push(result._id)
                            User.create(new_user, function(err, newuser){
                                if (err) throw err;
                                done(err, newuser)
                            })
                         
                        })
                       
                    
                      
                  }
            })
           
        } else {
            
            User.findOne({email: profile.emails[0].value}, function(err, currentuser){
                if(currentuser){
                    User.findOneAndUpdate({ email: currentuser.email }, { isValid:true, "gmail.accessToken": accessToken} , (err, updated_user) => {
                        console.log(updated_user);
                        done(err, updated_user);
                    })  
                  }
                  else{
                    var new_user_id = mongoose.Types.ObjectId();
                    var new_user = {
                        _id: new_user_id,
                        email: profile.emails[0].value,
                        role: "companyowner",
                        isValid:true,  
                        isCompanyOwner:true, 
                        group: [],
                        username:profile.displayName, 
                        gmail:{accessToken:accessToken, 
                        refreshToken:refreshToken}
                    }

                    // User.create({email: profile.emails[0].value, password:"any", role: "companyowner", isValid:true,  isCompanyOwner:true, username:profile.displayName, gmail:{accessToken:accessToken, refreshToken:refreshToken}},function(err,newuser){
                        Group.create({group_name: 'AllContacts',
                        template_id: default_template_id,
                        user: [new_user._id],
                        admin_id: new_user._id}, function(err, result){
                            new_user.group.push(result._id)
                            User.create(new_user, function(err, newuser){
                                if (err) throw err;
                                done(err, newuser)
                            })
                         
                        })
                       
                    
                      
                  }
            })
            
        }

})




//     } else {
//         User.findOne({email: profile.emails[0].value}).then(function(currentuser){
        
//         User.findOneAndUpdate({ email: profile.emails[0].value }, { "gmail.accessToken": accessToken} , (err, updated_user) => {
//             console.log(updated_user);
//             done(err, updated_user);
//         })
   
// })

passport.use(googleStratergy);
refresh.use(googleStratergy);
// passport.use(localStratergy);
