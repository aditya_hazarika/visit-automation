exports.roleCheck = function (role) {
    return function (req, res, next) {
        let user = req.user;
        // console.log(user)
        if (!user) {
            res.redirect('/login');
            return;
        }
        else if (!req.user.isValid) {
            res.redirect('/login/verifyEmail');
        }
        if (role === "compown" && user.isCompanyOwner === 'true') {
            next();
        }
        else if (role === "superAdmin" && user.isSuperAdmin === 'true') {
            next()
        }
        else if (user.isSuperAdmin === 'false' && user.isCompanyOwner === 'false' && role === 'usr') {
            next()
        }
        else {
            return res.redirect('/login/success');
        }
    }
}

exports.redirectUsers = function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }
    else if (!req.user.isValid) {
        res.redirect('/login/verifyEmail');
    }
    else {
        var user = req.user;
        // console.log(user.isSuperAdmin)
        if (user.isSuperAdmin === 'true') {
            return res.redirect('/superadmin');
        }
        else if (user.isCompanyOwner === 'true') {
            return res.redirect('/compown');
        }
        else if(!user.otp){
            return res.redirect('/change_password');
        }
        else {
            res.redirect('/user')
        }
    }
}

exports.router  
