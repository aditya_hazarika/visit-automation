const nodemailer = require("nodemailer");
const User = require("../models/user-model");
const keys = require("../config/keys");
const crypto = require("crypto");

exports.sendMail = function(requser, recipent, subject, content) {
  let transport;
  User.findOne(
    {
      $and: [{ company: requser.company }, { isCompanyOwner: true }]
    },
    (err, companyOwner) => {
      if (!(companyOwner.smtp.host || companyOwner.gmail.refreshToken))
        throw new Error("No email method was set");
      else if (companyOwner.smtp.host) {
        // let smtp = companyOwner.smtp;
        // var key = crypto.createDecipher("aes-128-cbc", "mypw");
        // var str = key.update(smtp.auth.pass, "hex", "utf8");
        // str += key.update.final("utf8");
        // smtp.auth.pass = str;
        transport = nodemailer.createTransport(companyOwner.smtp);
      } else {
        transport = nodemailer.createTransport({
          host: "smtp.gmail.com",
          port: 465,
          secure: true,
          auth: {
            type: "OAuth2",
            user: companyOwner.email,
            clientId: keys.google.client_id,
            clientSecret: keys.google.client_secret,
            accessToken: companyOwner.gmail.accessToken,
            refreshToken: companyOwner.gmail.refreshToken,
            expires: 1484314697598
          }
        });
      }
      transport.sendMail({
        to: recipent,
        from: companyOwner.email,
        subject: subject,
        html: content
      });
    }
  );
};
