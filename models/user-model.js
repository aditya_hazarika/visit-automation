const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs')

const UserSchema = new Schema({
    username: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    company: {
        type: String
    },
    isCompanyOwner: {
        type: Boolean
    },
    role: {
        type: String
    },
    gmail:
    {
        accessToken: {
            type: String,
        },
        refreshToken: {
            type: String,
        },
    },
    smtp: {
        host: String,
        port: Number,
        secure: Boolean,
        auth: {
            user: String,
            pass: String
        }
    },
    otp: {
        type: String,
    },
    isValid: {
        type: Boolean,
    },
    mobile_otp: {
        type: String,
    },
    mobile_isValid: {
        type: Boolean
    },
    group: {
        type: [String]
    }
})

UserSchema.pre('save', function (next) {
    var user = this;
    var SALT_FACTOR = 5;

    if(user.password){
        bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
            if (err) return err;
    
            bcrypt.hash(user.password, salt, (err, hash) => {
                if (err) return err;
                user.password = hash;
                next()
            })
        })
    }
    else{
        next()
    }
    
})

UserSchema.methods.comparePassword = function (passwordAttempt, callback) {
    bcrypt.compare(passwordAttempt, this.password, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch)
    })
}

UserSchema.methods.serializeUser = function (user, done) {
    done(null, user.id);
}
UserSchema.methods.deserializeUser = function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
}


const User = mongoose.model('user', UserSchema)

module.exports = User;
