const mongoose = require('mongoose')
const Schema = mongoose.Schema;
var passportLocalMongoose = require("passport-local-mongoose");


const TemplateSchema = new Schema({
    name:{
        type: String,
    },
    subject:{
        type :String,
    },
    template :{
        type: String
    },
    
    admin_id: {
        type: String
    }
})


const Template = mongoose.model('template', TemplateSchema)

module.exports = Template;