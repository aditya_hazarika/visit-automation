const mongoose = require('mongoose')
var passportLocalMongoose = require("passport-local-mongoose");

const Schema = mongoose.Schema;

const GroupSchema = new Schema({
    group_name:{
        type: String
    },
    template_id: {
        type: String
    },
    user: {
        type: [String]
    },
    admin_id: {
        type: String
    }
})

const Group = mongoose.model('group', GroupSchema)

module.exports = Group;