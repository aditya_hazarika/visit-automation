const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    company_name: {
        type: String,
    },
    name: {
        type: String
    },
    designation: {
        type: String
    },
    email: {
        type: [String]
    },
    phone_no: {
        type: String
    },
    website: {
        type: String
    },
    group: {
        type: [String]
    },
    extra_info: {
        type: String
    }
})




const Contact = mongoose.model('contact', ContactSchema)

module.exports = Contact;