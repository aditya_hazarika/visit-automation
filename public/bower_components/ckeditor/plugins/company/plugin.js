CKEDITOR.plugins.add( 'company', {
    icons: 'company',
    init: function( editor ) {
        editor.addCommand( 'insertCompany', {
            exec: function( editor ) {
                editor.insertHtml( '[company_name]' );
            }
        });
        editor.ui.addButton( 'company', {
            label: 'Insert company Name',
            command: 'insertCompany',
            toolbar: 'about,130'
        });
    }
});


