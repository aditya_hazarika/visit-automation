CKEDITOR.plugins.add( 'holder', {
    icons: 'holder',
    init: function( editor ) {
        editor.addCommand( 'insertHolderName', {
            exec: function( editor ) {
                editor.insertHtml( '[name]' );
            }
        });
        editor.ui.addButton( 'holder', {
            label: 'Insert Card-holder Name',
            command: 'insertHolderName',
            toolbar: 'about,100'
        });
    }
});


