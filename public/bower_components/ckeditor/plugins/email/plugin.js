CKEDITOR.plugins.add( 'email', {
    icons: 'email',
    init: function( editor ) {
        editor.addCommand( 'insertEmail', {
            exec: function( editor ) {
                editor.insertHtml( '[email]' );
            }
        });
        editor.ui.addButton( 'email', {
            label: 'Insert email-Id',
            command:'insertEmail',
            toolbar: 'about,160'
        });
    }
});


