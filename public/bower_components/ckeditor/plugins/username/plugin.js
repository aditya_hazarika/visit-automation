CKEDITOR.plugins.add( 'username', {
    icons: 'username',
    init: function( editor ) {
        editor.addCommand( 'insertUsername', {
            exec: function( editor ) {
                editor.insertHtml( '[username]' );
            }
        });
        editor.ui.addButton( 'username', {
            label: 'Insert Username',
            command: 'insertUsername',
            toolbar: 'about,120'
        });
    }
});


