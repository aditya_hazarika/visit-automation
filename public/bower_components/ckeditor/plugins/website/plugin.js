CKEDITOR.plugins.add( 'website', {
    icons: 'website',
    init: function( editor ) {
        editor.addCommand( 'insertWebsite', {
            exec: function( editor ) {
                editor.insertHtml( '[website]' );
            }
        });
        editor.ui.addButton( 'website', {
            label: 'Insert Company Website',
            command: 'insertWebsite',
            toolbar: 'about,150'
        });
    }
});


