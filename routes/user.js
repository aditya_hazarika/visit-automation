var express = require('express')
var app = express()
var Contact = require('../models/contact-model')
``
var Group = require('../models/group-model')
var Template = require('../models/template-model')
var User = require('../models/user-model')
var publicDir = require('path').join(__dirname, '../public');

var upload = require('express-fileupload')
var Contact = require('../models/contact-model')
const keys = require('../config/keys');

const mail = require('../controller/mail')


var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');


var path = require('path')

const fs = require('fs');
const convertor = require('@iarna/rtf-to-html');

const csv=require('csvtojson')

app.use(express.static(publicDir))

app.use(upload());


app.get('/', function (req, res) {
    var count = 0;
    for (let i = 0; i < req.user.group.length; i++) {
        Contact.find({group: req.user.group[i]}, (err, data)=>{
            count += data.length
        })        
    }

    User.findById(req.user._id, (err, group_no)=>{
        res.render('user/dashboard', {
            user: req.user,
            group_no: group_no.group.length,
            scanned_no: count
        })
    })
})



app.get('/groups', (req, res) => {
    let user = req.user;
    
    Group.find({ user: user._id }, (err, groups) => {
        console.log(groups);
        res.render('user/grps', {
            user: req.user,
            group_data: groups
        });
    })
})

app.get('/contacts', function (req, res) {
    var groups = req.user.group;
    console.log(groups);
    Contact.find({ group: { $in: groups } }, (err, contacts) => {
        res.render('user/contacts', {
            user: req.user,
            contacts: contacts
        });
    })
})

app.get('/:grpID/users', function (req, res) {
    Group.findById(req.params.grpID, (err, group) => {

        User.find({ group: group._id }, (err, Users) => {
            if (!err) {
                res.render('user/grpusers', {
                    user: req.user,
                    users_data: Users,
                    grp_name: group.group_name
                });
            }
        })
    })

})

app.get('/:grpID/contacts', function (req, res) {
    // console.log(req.params.grpID);
    User.findOne({$and: [{company: req.user.company}, {isCompanyOwner: "true"}]}, (err, admin_data)=>{
        Group.findById(req.params.grpID, (err, group_data)=>{
            Contact.find({ group: req.params.grpID }, (err, contacts) => {
                res.render('user/grpcontacts', {
                    contacts: contacts,
                    user: req.user,
                    groupId: req.params.grpID,
                    grp_name: group_data.group_name,
                    admin_data: admin_data
                });
            })
        })
    
    })
})

app.get('/:grpID/addcontacts', (req, res) => {
    Group.findById(req.params.grpID, (err, group_data)=>{
        res.render('user/addcontact',{
            user: req.user,
            group_id: req.params.grpID,
            grp_name: group_data.group_name
        })
    })
})
app.post('/:grpid/addcontact', function (req, res, next) {
    var temp_array = []

    temp_array = req.body.email.toString().split(',');
    User.findOne({$and: [{company: req.user.company}, {isCompanyOwner: "true"}]}, (err, admin_data)=>{
        var grp_array = []
        Group.findOne({$and: [{group_name: "AllContacts"}, {admin_id: admin_data._id}]}, (err, group_data)=>{
            grp_array.push(group_data._id, req.params.grpid)
            console.log(grp_array)
        
            var data = {
                company_name: req.body.company_name,
                name: req.body.name,
                email: temp_array,
                designation: req.body.designation,
                phone_no: req.body.phone_no,
                website: req.body.website,
                extra_info: req.body.extra_info,
                group: grp_array


            }

            temp_array.forEach(email =>{
                Contact.create(data, (err, contact_data) => {
                    Group.findById(req.params.grpid, (err, group_data) => {
                        Template.findById(group_data.template_id, (err, temp_data) => {
                            temp_data.template=temp_data.template.replace(/\[company_name]/g,contact_data.company_name);
                            temp_data.template=temp_data.template.replace(/\[name]/g,contact_data.name);
                            temp_data.template=temp_data.template.replace(/\[email]/g,email);
                            temp_data.template=temp_data.template.replace(/\[phone_no]/g,contact_data.phone_no);
                            temp_data.template=temp_data.template.replace(/\[website]/g,contact_data.website);
                            temp_data.template=temp_data.template.replace(/\[username]/g, admin_data.username);
            
                            mail.sendMail(req.user, email, temp_data.subject, temp_data.template)
                        });
                    })
                })
            })
        })   
    })
    res.redirect('/compown/'+req.params.grpid+'/contacts')
})
app.get('/:grpID/uploadcontacts', (req, res) => {
    Group.findById(req.params.grpID, (err, group_data)=>{
        res.render('user/uploadcsv',{
            user:req.user,
            group_id: req.params.grpID,
            grp_name: group_data.group_name
        })
    })
})

app.post('/:group_id/uploadcontacts', function (req, res) {
    console.log(req.files);

    User.findOne({$and: [{company: req.user.company}, {isCompanyOwner: "true"}]}, (err, docs)=>{
        //add group id into csv file
        if (req.files.upfile) {
            var file = req.files.upfile,
                name = file.name,
                type = file.mimetype;
            var uploadpath = './uploads/' + name;
            file.mv(uploadpath, function (err) {
                if (err) {
                    console.log("File Upload Failed", name, err);
                    res.send("Error Occured!")
                } else {

                    console.log("File Uploaded", name);

                    const csvFilePath = uploadpath

                    csv()
                        .fromFile(csvFilePath)
                        .then((jsonObj) => {

                            User.findOne({$and: [{company: req.user.company}, {isCompanyOwner: "true"}]}, (err,admin_data)=>{
                                var temp = [];
                                Group.findOne({$and: [{group_name: "AllContacts"}, {admin_id: admin_data._id}]}, (err, data)=>{
                                    temp.push(data._id, req.params.group_id)
                                    for (let i = 0; i < jsonObj.length; i++) {
                                        jsonObj[i].group = temp
                                        jsonObj[i].email = jsonObj[i].email.toString().split(',')                       
                                    }

                                    Contact.create(jsonObj, (err, upload_data)=>{
                                        if(err) throw err;
                                        upload_data.forEach(data=>{
                                            var email_array =[];
                                            email_array = data.email.toString().split(',')
            
                                            email_array.forEach(mails=>{
                                                Group.findById(req.params.group_id, (err, group_data) => {
                                                    Template.findById(group_data.template_id, (err, temp_data) => {
                                                        temp_data.template=temp_data.template.replace(/\[company_name]/g,upload_data.company_name);
                                                        temp_data.template=temp_data.template.replace(/\[name]/g,upload_data.name);
                                                        temp_data.template=temp_data.template.replace(/\[email]/g,mails);
                                                        temp_data.template=temp_data.template.replace(/\[phone_no]/g,upload_data.phone_no);
                                                        temp_data.template=temp_data.template.replace(/\[website]/g,upload_data.website);
                                                        temp_data.template=temp_data.template.replace(/\[username]/g, admin_data.username);
                                        
                                                        mail.sendMail(req.user, mails, temp_data.subject, temp_data.template)
                                                    });
                                                })
            
                                            })
                                        })
                                    })
                                })
                            })
                        })

                    console.log('Done! Uploading files')
                    res.redirect('/user/'+req.params.group_id+'/contacts')
                }
            });
        }else {
            console("No File selected !");
            res.redirect('/user/'+req.params.group_id+'/contacts')
        };
    })
    
})

//print more information of contact
app.get('/:contact_id/contact_extra_info', function(req, res){
    Contact.findById(req.params.contact_id, (err, contact_data)=>{
        if(err) throw err;
        res.render('user/contact_extra_info',{
            user: req.user,
            data: contact_data.extra_info
        })
       
    })
   
})




module.exports = app;
