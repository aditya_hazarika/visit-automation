var express = require('express')
var app = express()

var User = require('../models/user-model')
var Group = require('../models/group-model')
var Contact = require('../models/contact-model')

var publicDir = require('path').join(__dirname,'../public');
app.use(express.static(publicDir))


app.get('/',function(req,res){
    
    var count= 0;
    var count1 = 0;
    User.find({isCompanyOwner: "true"}, (err, data)=>{
        for (let i = 0; i < data.length; i++) {
            count+=data[i].group.length            
        }
    })
    Contact.find({_id: {$ne: null}}, (err, data)=>{
        count1=data.length
    })
    User.find({isCompanyOwner: "true"}, (err, company_data)=>{
        User.find({isSuperAdmin : "false"}, (err, user_data)=>{
            res.render('superadmin/dashboard',{
                user: req.user,
                company_no: company_data.length,
                user_no: user_data.length,
                group_no: count,
                scanned_no: count1
            })
        })
    })
})

app.get('/companies', function(req,res){
    var no_of_companies=[]
   
    var info = []

    User.find({isCompanyOwner : "true"}, (err, company_data)=>{
        for (let index = 0; index < company_data.length; index++) {
               no_of_companies.push(company_data[index]._id)
        }
        Group.find({$and:[{admin_id: no_of_companies},{group_name: 'AllContacts'}]}, function(err, docs){
        for (let i = 0; i < company_data.length; i++) {
        
            if(company_data[i]._id == docs[i].admin_id){
            var group_template = { company: company_data[i], user: docs[i] };
            info.push(group_template)
            }
            // console.log(info)
        }
        // console.log(info)
        res.render('superadmin/companies',{
            user: req.user,
            data: info
        })

        })
    })
})

app.get('/:admin_id/company_groups', function(req,res){
    User.find({_id: req.params.admin_id}, function(err, user_data){
        
        
        Group.find({_id:{$in: user_data[0].group}},function(err, group_data){
            res.render('superadmin/compgrps',{
                user: req.user,
                data: group_data,
                company_name: user_data[0].company
            })
        })
        
    })
})

app.get('/:adminid/company_users', function(req,res){
    Group.find({$and: [{admin_id: req.params.adminid},{group_name: 'AllContacts'}]},function(err, group_docs){
        // console.log(group_docs)
        User.find({_id: {$in: group_docs[0].user}},function(err,userdocs){
            res.render('superadmin/users',{
                user:req.user,
                data: userdocs,
            })
        })
    })
})

app.get('/:grpid/group_users', function(req,res){
    Group.find({_id: req.params.grpid}, function(err, groupdocs){
        User.find({_id:{$in: groupdocs[0].user}},function(err,userdocs){
            res.render('superadmin/grpusers', {
                user: req.user,
                data: userdocs,
                grp_name: groupdocs[0].group_name
            })
        })
    })
})

        
        

app.get('/grps', function(req,res){
    res.render('superadmin/grp',{
        user: req.user
    })
})

 
app.get('/users', function(req,res){
    User.find({isSuperAdmin: "false"}, (err, docs)=>{
        res.render('superadmin/users',{
            user: req.user,
            data: docs
        })

    })
})

app.get('/:admin_id/grps', function(req,res){
    Group.find({admin_id: req.params.admin_id}).then(function(err, groups){
        if(!err){
            res.render('superadmin/compgrps',{
                title: '',
                data: groups
            })

        }else throw err;
    })
})

app.get('/:grpID/users', function(req,res){

    User.find({group: {$in : req.params.grpID}}, (err, user_data)=>{
        res.render('superadmin/cmp_grp_users',{
            user: req.user,
            data: user_data
        })
    })
})

app.get('/:grpid/users',function(req,res){
    res.render('superadmin/cmp_grp_users')
})


module.exports =app;