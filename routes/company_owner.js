var express = require('express')
var app = express()
var Group = require('../models/group-model')
var Template = require('../models/template-model')
var deleteUser = require('../models/deleted-user-model')
var mongoose = require('mongoose');
const keys = require('../config/keys');


var User = require('../models/user-model')
var async = require('async')
var Contact = require('../models/contact-model')
var publicDir = require('path').join(__dirname, '../public');
var upload = require('express-fileupload')
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var path = require('path')

const fs = require('fs');
const convertor = require('@iarna/rtf-to-html');

const csv = require('csvtojson')
// var mail = require('../controller/mail');

var randomstring = require("randomstring");

const mail = require('../controller/mail')

var otp = require('otplib')
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'



app.use(express.static(publicDir))
app.use(upload());

var default_template_id;
Template.findOne({ name: "default_template" }, (err, doc) => {
    if (err) throw err;
    default_template_id = doc._id;
});


app.get('/', function (req, res) {
    Group.find({ $and: [{ admin_id: req.user._id }, { group_name: 'AllContacts' }] }, (err, groups) => {
        Template.find({ admin_id: req.user._id }, (err, temp_data) => {
            var grps = { userlen: groups[0].user.length, templen: temp_data.length }
            console.log(res.statusCode)
            res.render('comp_own/dashboard', {
                user: req.user,
                data: grps,
                requiresMail: (!(req.user.smtp.host || req.user.gmail.refreshToken))
            })
        })

    })

})

app.get('/grp', function (req, res) {
    Group.find({ admin_id: req.user._id }, (err, groups) => {
        if (err) throw err;
        else {
            Template.find({ $or: [{ admin_id: req.user.id }, { name: 'default_template' }] }, (err, templates) => {
                var groups_templates = []
                for (let i = 0; i < groups.length; i++) {
                    for (let j = 0; j < templates.length; j++) {
                        if (groups[i].template_id == templates[j]._id) {
                            // console.log("matched")
                            var group_template = { group: groups[i], template: templates[j] };
                            groups_templates.push(group_template);

                        }
                    }

                }
                // console.log(groups_templates)
                console.log(res.statusCode)

                res.render('comp_own/grp', {
                    user: req.user,
                    data: groups_templates,
                    templates: templates
                })

            })

        }
    })
})
app.put('/:group_id/assign_template', function (req, res) {
    Group.findByIdAndUpdate(req.params.group_id, { template_id: req.body.template_id }, (err, result) => {
        console.log(res.statusCode)
        if (err) throw err;
        else {


            res.redirect('/compown/grp')
        }
    })
})

app.get('/user', function (req, res) {
    User.find({ company: req.user.company }, function (err, user_data) {
        // console.log(user_data)
        console.log(res.statusCode)

        res.render('comp_own/user', {
            user: req.user,
            data: user_data
        })
    })
})

app.get('/user/:userID/makeadmin', (req, res) => {
    User.findById(req.params.userID, (err, user) => {
        User.find({ company: user.company, isCompanyOwner: true }, (err, companyOwner) => {
            User.findByIdAndUpdate(req.params.userID, { role: "companyowner", group: companyOwner.group }, (err, res) => {
                Group.updateMany({ _id: { $in: companyOwner.group } }, { push: { user: users_to_be_added } }, (err, raw) => { res.redirect('/compown/user') })
            })
        })
    })
})

app.get('/user/:userID/removeadmin', (req, res) => {
    User.findByIdAndUpdate(req.params.userID, { role: "user" }, (err, result) => {
        if (!err)
            res.redirect('/compown/user');
    });
})

app.get('/:userId/delete_user', function (req, res) {
    console.log(req.params.userId)
    Group.find({ user: { $in: req.params.userId } }, function (err, grp_data) {
        for (let i = 0; i < grp_data.length; i++) {
            Group.findByIdAndUpdate(grp_data[i]._id, { $pull: { user: req.params.userId } }, function (err, grpinfo) {
                console.log(grpinfo)
            })
        }
        User.findById(req.params.userId, (err, data)=>{
            var delete_data={
                username : data.username,
                email: data.email
            }
            deleteUser.create(delete_data);
            User.findByIdAndRemove(req.params.userId).then(function (any) {
                console.log(res.statusCode)
    
                res.redirect('/compown/user')
            })
        })
        
    })

})

app.get('/contacts', function (req, res) {
    // console.log(req.user.group)
    Contact.find({ group: { $in: req.user.group } }, (err, docs) => {
        // console.log(docs)
        if (err) throw err;
        console.log(res.statusCode)

        res.render('comp_own/contact', {
            user: req.user,
            data: docs
        })
    })
})

app.get('/template', function (req, res) {
    // res.render('comp_own/template')
    Template.find({ $or: [{ admin_id: req.user.id }, { name: 'default_template' }] }, (err, template_data) => {
        console.log(res.statusCode)

        res.render('comp_own/template', {
            user: req.user,
            data: template_data,
            admin_id: req.user._id
        })
    })
})

app.post('/addtemp', function (req, res) {
    var data = {
        name: req.body.name,
        subject: req.body.subject,
        template: req.body.template,
        admin_id: req.user._id
    }
if(data.name != "default_template"){
    Template.create(data, function (err) {
        req.flash("success", "Template Created successfully")

        res.redirect('/compown/template')
    })
}
else{

    req.flash("error", "Cannot create template named 'default_template' try some different name")
    res.redirect('/compown/template')

}

})

app.get('/:template_id/show_template', function (req, res) {

    Template.findById(req.params.template_id, function (err, template_data) {
        // console.log(template_data)
        res.render('comp_own/show_template', {
            data: template_data,
            user: req.user,
        })
    })
})


app.get('/:template_id/edit_template', function (req, res) {
    Template.find({ _id: req.params.template_id }, function (err, template_data) {
        console.log(template_data)
        res.render('comp_own/edit_template', {
            data: template_data,
            user: req.user,
            template_id: req.params.template_id
        })
    })
})

app.put('/:template_id/edit_template', function (req, res) {

    Template.findByIdAndUpdate({ _id: req.params.template_id }, { $set: { subject: req.body.subject, template: req.body.template } }, function (err, template_data) {
        console.log(res.statusCode)

        res.redirect('/compown/template')
    })

})

app.get('/usrgrps', function (req, res) {

    res.render('comp_own/usr_grp')
})

app.get('/:grpid/addusersgrp', function (req, res) {

    Group.findById(req.params.grpid, (err, group) => {
        User.find({ $and: [{ company: req.user.company }, { group: { $nin: [req.params.grpid] } }, { isCompanyOwner: false }] }, (err, user_data) => {

            res.render('comp_own/addgrpusers', {
                user: req.user,
                data: user_data,
                grp_id: req.params.grpid,
                grp_name: group.group_name
            })

        })
    })
})

app.put('/:grpid/addusersgrp', function (req, res) {
    let users_to_be_added = []
    users_to_be_added = req.body.user.toString().split(',');
    console.log(users_to_be_added);

    Group.findByIdAndUpdate(req.params.grpid, { $push: { user: users_to_be_added } }, (err, result) => {
        if (err) throw err;
        if (!err) {
            users_to_be_added.forEach(user => {
                User.findByIdAndUpdate(user, { $push: { group: req.params.grpid } }, (err, result) => {
                    if (err) throw err;

                    console.log("added");
                })
            });
            res.redirect('/compown/' + req.params.grpid + '/users');
        }
    })

})

app.get('/:grpid/users', function (req, res) {



    Group.findById(req.params.grpid, (err, group) => {
        if (!err) {
            let username = []
            User.find({ _id: { $in: group.user } }, (err, docs) => {
                if (err) throw err;
                console.log(group.user)
                res.render('comp_own/grpusers', {
                    user: req.user,
                    data: docs,
                    group_id: req.params.grpid,
                    grp_name: group.group_name
                })
            })
        }
    })

})

app.get('/:grpid/contacts', function (req, res) {
    Group.findById(req.params.grpid, (err, grp_name) => {
        Contact.find({ group: req.params.grpid }, (err, contact_data) => {
            res.render('comp_own/grpcontacts', {
                user: req.user,
                data: contact_data,
                group_id: req.params.grpid,
                grp_name: grp_name.group_name,
                requiresMail: (!(req.user.smtp.host || req.user.gmail.refreshToken))

            })
        })
    })
})


app.get('/addgrp', function (req, res) {
    res.render('comp_own/addgrp', {
        user: req.user
    })
})

app.post('/addgrp', function (req, res) {

    var data = {
        group_name: req.body.group_name,
        template_id: default_template_id,
        user: [req.user._id],
        admin_id: req.user.id
    }
    if(data.group_name!= "AllContacts"){
    Group.create(data, function (err, group_data) {
        User.findByIdAndUpdate(req.user._id, { $push: { group: group_data._id } }, (err, result) => {
            if (err) throw err;
            console.log(res.statusCode)
            req.flash('success', "Group created successfully")

            res.redirect('/compown/grp');
        })

    })
}
else{
    req.flash('error', "Cannot create group named 'AllContacts' try different Name")
    res.redirect('/compown/grp');

}
})




app.get('/addtemp', function (req, res) {
    console.log(res.statusCode)

    res.render('comp_own/addtemp', {
        user: req.user,
        name: '',
        admin_id: req.user._id
    })
})

app.get('/:grpid/addcntct', function (req, res) {
    Group.findById(req.params.grpid, (err, grp_data) => {
        res.render('comp_own/addcntct', {
            user: req.user,
            grp_id: req.params.grpid,
            grp_name: grp_data.group_name
        })
    })
})
app.post('/:grpid/addcntct', function (req, res, next) {

    var temp_array = []

    temp_array = req.body.email.toString().split(',');

    var grp_array = []
    Group.findOne({ $and: [{ group_name: "AllContacts" }, { admin_id: req.user._id }] }, (err, group_data) => {
        grp_array.push(group_data._id, req.params.grpid)
        console.log(grp_array)

        var data = {
            company_name: req.body.company_name,
            name: req.body.name,
            email: temp_array,
            designation: req.body.designation,
            phone_no: req.body.phone_no,
            website: req.body.website,
            extra_info: req.body.extra_info,
            group: grp_array


        }

        temp_array.forEach(email => {
            Contact.create(data, (err, contact_data) => {
                Group.findById(req.params.grpid, (err, group_data) => {
                    Template.findById(group_data.template_id, (err, temp_data) => {
                        temp_data.template = temp_data.template.replace(/\[company_name]/g, contact_data.company_name);
                        temp_data.template = temp_data.template.replace(/\[name]/g, contact_data.name);
                        temp_data.template = temp_data.template.replace(/\[email]/g, email);
                        temp_data.template = temp_data.template.replace(/\[phone_no]/g, contact_data.phone_no);
                        temp_data.template = temp_data.template.replace(/\[website]/g, contact_data.website);
                        temp_data.template = temp_data.template.replace(/\[username]/g, req.user.username);

                        mail.sendMail(req.user, email, temp_data.subject, temp_data.template)
                    });
                })
            })
        })
    })
    res.redirect('/compown/' + req.params.grpid + '/contacts')
})






app.get('/defcontacts', function (req, res) {
    res.render('comp_own/defcontacts')
})

app.get('/adduser', function (req, res) {
    res.render('comp_own/adduser', {
        user: req.user
    })
})

app.get('/:grpid/uploadcsv', function (req, res) {

    Group.findById(req.params.grpid, (err, grp_data) => {
        res.render('comp_own/upldcsv', {
            user: req.user,
            group_id: req.params.grpid,
            grp_name: grp_data.group_name
        })
    })
})



app.post('/:group_id/uploadcsv', function (req, res) {
    // console.log(req.files);

    //add group id into csv file
    if (req.files.upfile) {
        var file = req.files.upfile,
            name = file.name,
            type = file.mimetype;
        var uploadpath = './uploads/' + name;
        file.mv(uploadpath, function (err) {
            if (err) {
                console.log("File Upload Failed", name, err);
                res.send("Error Occured!")
            } else {

                console.log("File Uploaded", name);

                const csvFilePath = uploadpath

                csv()
                    .fromFile(csvFilePath)
                    .then((jsonObj) => {

                        var temp = [];
                        Group.findOne({ $and: [{ group_name: "AllContacts" }, { admin_id: req.user._id }] }, (err, data) => {
                            temp.push(data._id, req.params.group_id)
                            for (let i = 0; i < jsonObj.length; i++) {
                                jsonObj[i].group = temp
                                jsonObj[i].email = jsonObj[i].email.toString().split(',')
                            }

                            Contact.create(jsonObj, (err, upload_data) => {
                                if (err) throw err;
                                upload_data.forEach(data => {
                                    var email_array = [];
                                    email_array = data.email.toString().split(',')

                                    email_array.forEach(mails => {
                                        Group.findById(req.params.group_id, (err, group_data) => {
                                            Template.findById(group_data.template_id, (err, temp_data) => {
                                                temp_data.template = temp_data.template.replace(/\[company_name]/g, upload_data.company_name);
                                                temp_data.template = temp_data.template.replace(/\[name]/g, upload_data.name);
                                                temp_data.template = temp_data.template.replace(/\[email]/g, mails);
                                                temp_data.template = temp_data.template.replace(/\[phone_no]/g, upload_data.phone_no);
                                                temp_data.template = temp_data.template.replace(/\[website]/g, upload_data.website);
                                                temp_data.template = temp_data.template.replace(/\[username]/g, req.user.username);

                                                mail.sendMail(req.user, mails, temp_data.subject, temp_data.template)
                                            });
                                        })

                                    })
                                })
                            })
                        })


                    })
                console.log('Done! Uploading files')
                res.redirect('/compown/' + req.params.group_id + '/contacts')
            }
        });
    }

    else {
        console.log("No File selected !");
        res.redirect('/compown/' + req.params.group_id + '/contacts')
    };
})






app.post('/adduser', function (req, res) {

    
    // var token = otp.authenticator.generate(secret)
    var username_array = []
    username_array = req.body.username.toString().split(',');
    var email_array = []
    email_array = req.body.email.toString().split(',');

    let i = 0;

    var exist_username = [];
    var exist_email = [];

    username_array.forEach(usernames=>{
        var generated_password = randomstring.generate(7);
        var user_data = {
            username: usernames,
            email: email_array[i],
            company: req.user.company,
            password: generated_password,
            isCompanyOwner: false,
            role: "user",
            group: req.user.group[0],
            isValid: false,
        }
    
        User.findOne({username: user_data.username}, (err, result)=>{
            if(result){
                exist_username.push(result.username)
            }else{
                User.findOne({email: user_data.email}, (err, data)=>{
                    if(data){
                        exist_email.push(data.email) 
                    }else{
                        User.create(user_data, (err, new_user_data)=>{
                            var transport = nodemailer.createTransport({
                                host: 'smtp.ethereal.email',
                                port: 587,
                                secure: false,
                                auth: {
                                    user: "l7kaiuofhcxc4oe2@ethereal.email",
                                    pass: "NqsXSGeYCSttjbDPnu"
                                }
                            });
                            let mailOptions = {
                                from: req.user.email,
                                to: new_user_data.email,
                                subject: 'Login Link',
                                // text: req.body.message,
                                text: `http://localhost:3000/login/  Your EmailID: ${new_user_data.email} and Your Password : ${generated_password}`,
                            };
                            transport.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                }
                                console.log('Message sent: %s', info.messageId);
                                // Preview only available when sending through an Ethereal account
                                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                    
                            });
                            Group.findByIdAndUpdate(req.user.group[0], { $push: { user: new_user_data._id } }, (err, group_data) => {
                                if (err) throw err;
                                console.log('Group Updated');
                            })
                        })
                    }
                })
            }
        })
        i++;
    })

    if(exist_username){
        req.flash('error', "Username already exist")
        res.redirect("/compown/adduser") 
    }else if(exist_email){
        req.flash('error', "EmailID already exist")
        res.redirect("/compown/adduser")
    }else{
        res.redirect('/compown/user')
    }
    
})


// delete template
app.delete('/template/:template_id', function (req, res) {

    console.log(req.params.template_id)

    Template.findByIdAndDelete(req.params.template_id, (err, deleted) => {
        console.log(res.statusCode)

        Template.findOne({ name: 'default_template' }, function (err, info) {
            console.log('deftemp:' + info)
            Group.findOneAndUpdate({ template_id: req.params.template_id }, { $set: { template_id: info._id } }, function (err, grp_data) {

                res.redirect('/compown/template')
            })

        })


    })
})


// //delete group
app.delete('/:group_id/remove_group', function (req, res) {

    User.find({ group: { $in: req.params.group_id } }, function (err, info) {
        console.log(info)
        for (let i = 0; i < info.length; i++) {
            // users_id.push(user_data[i]._id)
            User.findByIdAndUpdate(info[i]._id, { $pull: { group: req.params.group_id } }, function (err, userinfo) {
                console.log(res.statusCode)

                console.log(userinfo)


            })

        }
        Group.findByIdAndRemove(req.params.group_id).then(function (any) {
            res.redirect('/compown/grp')

        })
    })

})

//remove user from grp

app.get('/:group_id/:user_id/remove_user', function (req, res) {
    User.findByIdAndUpdate(req.params.user_id, { $pull: { group: req.params.group_id } }, function (err, any) {
        Group.findByIdAndUpdate(req.params.group_id, { $pull: { user: req.params.user_id } }, function (err, any2) {
            res.redirect('/compown/' + req.params.group_id + '/users')

        })

    })
})

//print more information of contact
app.get('/:contact_id/contact_extra_info', function (req, res) {
    Contact.findById(req.params.contact_id, (err, contact_data) => {
        if (err) throw err;

        res.render('comp_own/contact_extra_info', {
            user: req.user,
            data: contact_data.extra_info
        })

    })

})
//Route for Company Owner settings

app.get('/settings', (req, res) => {
    User.findOne(
        {
            $and: [{ company: requser.company }, { isCompanyOwner: true }]
        }, (err, companyOwner) => {
            if (!err) {
                res.render('comp_own/settings', {
                    user: req.user,
                    error: null,
                    smtp: companyOwner.smtp,
                })
            }
        })
})

const crypto = require('crypto');
app.post('/settings', (req, res) => {
    // let key = crypto.createCipher('aes-256-ccm', 'mypw') //Change pw here
    // var password = key.update(req.body.smtppassword);
    // password += key.update.final('hex')
    var smptOptions = {
        host: req.body.smtpaddress,
        port: req.body.smtpport,
        secure: req.body.smtpsecure === 'true', //Conver smtpsecure from string to Boolean
        auth: {
            user: req.body.smtpusername,
            pass: req.body.smtppassword
        }
    }
    //Validate the new smtpSettings before storing them in the databases
    nodemailer.createTransport(smptOptions).verify((err, success) => {
        if (err) {
            //Show error if it occurs
            res.render('comp_own/settings', {
                user: req.user,
                error: `There was an error validating your SMTP settings, please try again with valid SMTP details ${err}`,
                smtp: smptOptions
            })
        }
        else {
            User.findOneAndUpdate({
                $and: [{ company: requser.company }, { isCompanyOwner: true }]
            },
                { smtp: smptOptions }, (err, updated_user) => {
                    // console.log('Updated smtp settings:' + updated_user);
                    res.redirect('/compown/settings');
                })
        }
    })
})
module.exports = app;
