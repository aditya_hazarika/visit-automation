# Visit Automation Mobile API

Below are the routes for the mobile API with their expected requests and resposes. API root is at `localhost/api`

## `/login`

### Request

```
req = {
    "email":"String",
    "password":"String"
}
```

### Response

```
res = {
    "success":true or false //Whether login was successful
    "msg":"Error message" //Empety on succeful login
    "token": "JWT [token]" //JWToken on successful login, empty otherwise
}
```

## `/groups`

### Request

```
req = {
    //Can be empty
}
```

### Response

```
res = {
    "success":true or false
    "groups":[] //Array of groups logged in user is part of
}
```

## GET `/:groupID/contacts`

### Request

```
req = {
    //Can be empty
}
```

### Response

```
res = {
    "success":true or false
    "contacts":[] //Array of contacts
}
```

## POST `/:groupID/contacts`

### Request

```
req = {
    companyName: String,
    name: String,
    designation: String,
    email: [String],
    phoneNo: String,
    group: String
}
```

### Response

```
res = {
    "success":true or false //Wheter user was added
    "msg":String //Error message
}
```
