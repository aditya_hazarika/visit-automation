const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const config = require("../../config/keys");
const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const router = express.Router();
var otplib = require('otplib')
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'
var mongoose = require('mongoose');
const User = require("../../models/user-model");
const Group = require("../../models/group-model");
const Contact = require("../../models/contact-model");
const mail = require('../../controller/mail')

router.use(bodyParser.json());
router.use(cors());
//require("../../config/passport-jwt-setup")(passport);

let opts = {};
opts.jwtFromRequest = ExtractJwt.fromExtractors([ExtractJwt.fromAuthHeaderAsBearerToken(),ExtractJwt.fromBodyField('jwt')]);
opts.secretOrKey = config.jwtSecret;
passport.use(
  "myjwt",
  new JwtStrategy(opts, (jwt_payload, done) => {
    User.findById(jwt_payload.data._id, (err, user) => {
      if (err) {
        return done(err, false);
      }

      if (user) {
        return done(null, user);
      } else {
        return done(null, false);
      }
    });
  })
);

router.post("/login", (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  User.findOne({ email: email }, (err, user) => {
    if (err) {
      res.status(500);
      return res.json({
        status: 500,
        message: "Server error occured",
        data: null
      });
    }
    if (!user) {
      return res.json({
        status: 204,
        message: "User was not found, please check the email address",
        data: null
      });
    } else {
      if (!user.isValid) {
        res.status(203)
        return res.json({
          status: 203,
          message: "Registration not complete",
          data: null
        });
      }
      user.comparePassword(password, (err, isMatch) => {
        if (err) {
          res.status(500)
          return res.json({
            status: 500,
            message: "Server error occured",
            data: null
          });
        }
        if (isMatch) {
          const token = jwt.sign({ data: user }, config.jwtSecret);
          var role = "user";
          if (user.isCompanyOwner === "true") role = "admin";
          if (user.isSuperAdmin === "true") role = "superadmin";
          return res.json({
            status: 200,
            message: "Successfully logged in",
            data: {
              token: "JWT " + token,
              userID: user.id,
              userRole: role
            }
          });
        } else {
          res.status = 202;
          res.json({ status: 202, message: "Incorrect password", data: null });
        }
      });
    }
  });
});

router.get("/groups",
  passport.authenticate("myjwt"),
  (req, res) => {
    Group.find({ _id: { $in: req.user.group } }, (err, groups) => {
      if (err) {
        if (err) throw err;
        res.status(500);
        return res.json({
          status: 500,
          message: err,
          data: null
        });
      }
      var groupsJson=[];
      groups.forEach(group=>{
        var groupJson={};
        groupJson.id=group._id;
        groupJson.group_name=group.group_name
        groupsJson.push(groupJson);
      })
      res.json({
        status: 200,
        message: "Group list of user",
        data: groupsJson
      });
    });
  });

router.get("/:groupID/contacts",
  passport.authenticate("myjwt"),
  (req, res) => {
    Contact.find({ group: req.params.groupID }, (err, contacts) => {
      if (err) {
        res.status(500);
        return res.json({ status: 500, message: err, data: null });
      }
      res.json({ status: 200, message: "Contacts found", contacts: contacts });
    });
  });

router.post(
  "/:groupID/contacts",
  passport.authenticate("myjwt"),
  (req, res) => {
    if (req.body.user.group.includes(req.params.groupID)) {

      var temp_array = []

      temp_array = req.body.email.toString().split(',');

      var data = {
        company_name: req.body.company_name,
        name: req.body.name,
        email: temp_array,
        designation: req.body.designation,
        phone_no: req.body.phone_no,
        website: req.body.website,
        extra_info: req.body.extra_info,
        group: req.params.groupID


      }

      temp_array.forEach(email => {
        Contact.create(data, (err, contact_data) => {
          Group.findById(req.params.groupID, (err, group_data) => {
            Template.findById(group_data.template_id, (err, temp_data) => {
              temp_data.template = temp_data.template.replace(/\[company_name]/g, contact_data.company_name);
              temp_data.template = temp_data.template.replace(/\[name]/g, contact_data.name);
              temp_data.template = temp_data.template.replace(/\[email]/g, email);
              temp_data.template = temp_data.template.replace(/\[phone_no]/g, contact_data.phone_no);
              temp_data.template = temp_data.template.replace(/\[website]/g, contact_data.website);
              temp_data.template = temp_data.template.replace(/\[username]/g, req.body.user.username);

              mail.sendMail(req.body.user, email, temp_data.subject, temp_data.template)
            });
          })
        })
      })
      res.json({ status: 201, message: "Contacts Created and mail sent", data: null });


    } else {
      res.json({ status: 403, message: "User not part of the group", data: null });
    }
  }
);



const Template = require('../../models/template-model');
router.post('/groups',
  passport.authenticate("myjwt"),
  (req, res) => {
    if (req.body.group_name) {
      Template.find({ name: 'default_template' }, (err, template) => {
        if (template) {
          Group.create({
            group_name: req.body.group_name,
            template_id: template._id,
            user: [req.body.user.id],
            admin_id: req.body.user.id
          }, (err, group_data) => {
            if (err) throw err;
            res.json({ status: 201, message: "Group Created", data: { group_id: group_data._id } });

          })
        }
      })

    }
    else {
      res.json({ status: 500, message: "SErver error", data: null });

    }
  })
var default_template_id;
Template.findOne({ name: "default_template" }, (err, doc) => {
  if (err) throw err;
  default_template_id = doc._id;
});

router.post('/register', (req, res) => {
  var token = otplib.authenticator.generate(secret)
  console.log(token)

  var new_user_id = mongoose.Types.ObjectId();
  var new_user = {
    _id: new_user_id,
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    company: req.body.company,
    isCompanyOwner: false,
    isSuperAdmin: false,
    otp: token,
    isValid: false,
    group: []
  }

  User.findOne({ username: new_user.username }, function (err, result) {
    // console.log(result)
    if (result) {
      res.json({
        status: 400,
        message: "User name already exists",
        data: "null"
      })
    }
    else {
      User.findOne({ email: new_user.email }, function (err, result) {
        console.log(result);
        if (result) {
          res.json({
            status: 401,
            message: "Email already registered",
            data: null,
          })
        }
        else {
          User.findOne({
            $and: [
              { company: new_user.company },
              { isCompanyOwner: true }
            ]
          }, function (err, companyOwner) {
            //Procedure to create a new normal user
            if (err) return console.log(err)
            if (companyOwner) {
              console.log("//Creating normal user")
              new_user.group.push(companyOwner.group[0])
              User.create(new_user, (err, added_user) => {
                Group.findByIdAndUpdate(added_user.group[0], { $push: { user: new_user._id } }, (err, updated_group) => {
                  if (err) throw err;
                  console.log('Updated group:' + updated_group);

                })
              })

            }
            else {
              console.log("Creating company owner")
              //create new group
              Group.create({
                group_name: 'AllContacts',
                template_id: default_template_id,
                user: [new_user_id],
                admin_id: new_user_id
              }, (err, companyGroup) => {
                if (err) throw err;
                new_user.isCompanyOwner = true;
                new_user.group.push(companyGroup._id)
                User.create(new_user);
              });
              Group.create()
            }
            var nodemailer = require('nodemailer');
            var transport = nodemailer.createTransport({
              host: "smtp.mailtrap.io",
              port: 2525,
              auth: {
                user: "145aa399c68c76",
                pass: "86a80afd21a1b9"
              }
            });
            var mailoptions = {
              from: 'superadmim@nimap.com',
              to: new_user.email,
              subject: 'Activate your account',
              text: `Your OTP is ${token}`
            }

            transport.sendMail(mailoptions, (err, info) => {
              console.log("OTP Sent")
            })

            res.json({
              status: 200,
              message: "Registration successful",
              data: null
            });
          })
        }
      })
    }

  })
})

router.get('/resend_otp',
  passport.authenticate("myjwt"),
  (req, res) => {
    var new_otp = otplib.totp.generate(secret)
    User.findByIdAndUpdate(req.params.user_id, { otp: new_otp }, (err, user_data) => {
      if (!err) {
        var nodemailer = require('nodemailer');
        var transport = nodemailer.createTransport({
          host: "smtp.mailtrap.io",
          port: 2525,
          auth: {
            user: "145aa399c68c76",
            pass: "86a80afd21a1b9"
          }
        });
        var mailoptions = {
          from: 'superadmim@nimap.com',
          to: user_data.email,
          subject: 'Activate your account',
          text: `Your NEW OTP is ${new_otp}`
        }

        transport.sendMail(mailoptions, (err, info) => {
          console.log("new OTP Sent")
        })
        res.json({ status: 200, msg: "OTP sent", data: null })

      } else {
        res.json({
          status: 500,
          msg: "Otp not sent, server error",
          data: null
        })

      }
    })

  })

module.exports = router;
