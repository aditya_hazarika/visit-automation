var express = require('express')
var passport = require('passport')
var LocalStratergy = require('passport-local').Strategy;
var User = require('../models/user-model')
var AuthenticationController = require('../controller/authentication');
//var Strategy = require('../config/passport-setup');

//const bcrypt = require('bcryptjs');

var otplib = require('otplib')
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'

// import totp from 'otplib/totp';
// const TOTP = totp.TOTP


var app = express()
var flash = require('connect-flash');

app.use(passport.initialize());
app.use(passport.session());
var publicDir = require('path').join(__dirname, '../public');
app.use(express.static(publicDir))
app.use(flash())
passport.serializeUser(function (user, done) {
    done(null, user.id);
})

passport.deserializeUser(function (id, done) {
    User.findById(id).then(function (user) {
        done(null, user)
    })
})


//Setup passport
var localStratergy = new LocalStratergy({
    usernameField: 'email',
    session: true,

}, function (email, password, done) {
    // console.log("Inline stratergy triggered");
    User.findOne({ email: email }, (err, user) => {
        if (err) return done(err);
        if (!user) {
            return done(null, false, { message: "Login failed." });
        }

        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                return done(err);
            }
            if (!isMatch) {
                return done(null, false, { message: "Login failed." });
            }
            return done(null, user);
        })
    })
})
passport.use(localStratergy);

app.get("/", function (req, res) {
    if (req.user) {
        res.redirect('/login/success');
    }
    else {
        res.render("login");
    }
})
app.get('/success', AuthenticationController.redirectUsers, (req, res) => {
    console.log("successful login")
})

// middleware
app.post("/", passport.authenticate('local', {
    failureRedirect: '/login',
    failureFlash: true,
    successRedirect: '/login/success'
}), function (req, res) {
    if(req.user){
        req.flash('success','logged in successfully')
    }
    else{
        req.flash('fail','Invalid Login Credentials')
    }
});

app.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});

app.get('/google', passport.authenticate('google', {
    accessType: 'offline',
    scope: [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/plus.me',
        'https://mail.google.com/',
        'https://www.googleapis.com/auth/gmail.modify',
        'https://www.googleapis.com/auth/gmail.compose',
        'https://www.googleapis.com/auth/gmail.send',
    ]
}, function (req, res) {
    console.log('this should not be called');
}))

app.get('/google/callback', passport.authenticate('google', {
    failureRedirect: "/login"
}), (req, res) => {
    res.redirect('/login/success');
})

app.get('/verifyEmail', (req, res) => {
    res.render('verify', {
        user: req.user,
        status:null
    });
})

app.post('/verifyEmail', (req, res) => {
    // console.log(req.body);

    const checking = otplib.totp.check(req.body.otp, secret)

    if(checking == true){
        User.findByIdAndUpdate(req.user._id, {isValid: true}, (err, user)=>{
            res.redirect('/login/success')
        })
    }else{
        User.findById(req.user._id, (err, data)=>{
            res.render('verify',{
                user: req.user,
                status: "Invalid OTP"
            })
        })
    }
})
module.exports = app;
