const router = require('express').Router();
const authCheck = function(req,res,next){
    if(!req.user){
        //if user is not logged in
            res.redirect('index');
    }else{
        next();
    }
}
router.get('/', authCheck, function(req,res){
   // res.send('You are successfully logged in'+req.user.username)
   res.render('index',{user: req.user})
})

module.exports = router;