var express = require('express')
var passport = require('passport')
var User = require('../models/user-model')
var mongoose = require('mongoose');
var otplib = require('otplib')
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'


var app = express()

var publicDir = require('path').join(__dirname, '../public');
app.use(express.static(publicDir))


app.get('/', function(req, res){
    res.render('change_password', {
        user: req.user,

    })
})

app.post('/:email', function(req, res){

    var token = otplib.authenticator.generate(secret)

    var user = {
        _id : req.user._id,
        username : req.body.username,
        email: req.params.email,
        company: req.user.company,
        password: req.body.password,
        isCompanyOwner: false,
        isSuperAdmin: false,
        otp: token,
        isValid: true,
        group: req.user.group

    }

    // console.log('data: ',user)

    User.findByIdAndDelete(req.user._id, (err, docs)=>{
        User.create(user, (err, user_data)=>{
            res.redirect('/login/success')
        })
    })
})


module.exports = app;
