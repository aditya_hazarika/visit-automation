const bcrypt = require('bcryptjs');

var express = require('express')
var passport = require('passport')
var User = require('../models/user-model')
var mongoose = require('mongoose');
// var flash = require('flash')

var Template = require('../models/template-model')
var Group = require('../models/group-model')

//
const jwt = require('jsonwebtoken');


var otplib = require('otplib')
const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD'

// import totp from 'otplib/totp';
// const TOTP = totp.TOTP


var app = express()

var publicDir = require('path').join(__dirname, '../public');
app.use(express.static(publicDir))

passport.serializeUser(User);
passport.deserializeUser(User);

app.use(passport.initialize());
app.use(passport.session());

var default_template_id;
Template.findOne({ name: "default_template" }, (err, doc) => {
    if (err) throw err;
    default_template_id = doc._id;
});


app.get("/", function (req, res) {
    if (req.user) {
        res.redirect('/login/success');
    }
    else {
        res.render("register");
    }
});

app.post('/', function (req, res) {

    const token = otplib.totp.generate(secret)
    console.log(token)

    // const token = new TOTP();
    // console.log(token)

    var new_user_id = mongoose.Types.ObjectId();
    var new_user = {
        _id: new_user_id,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        company: req.body.company,
        isCompanyOwner: false,
        isSuperAdmin: false,
        otp: token,
        isValid: false,
        group: []
    }
    var nodemailer = require('nodemailer');
    var transport = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
            user: "145aa399c68c76",
            pass: "86a80afd21a1b9"
        }
    });
    var mailoptions={
        from:'superadmim@nimap.com',
        to : new_user.email,
        subject: 'Activate your account',
        text: `Your OTP is ${token}`
    }

    transport.sendMail(mailoptions,(err,info)=>{
        console.log("OTP Sent")
    })

    User.findOne({ username: new_user.username }, function (err, result) {
        // console.log(result)
        
        if (result) {req.flash('error', 'Username already exist')
        res.redirect('/register')}
        else {
            User.findOne({ email: new_user.email }, function (err, result) {
                console.log(result);
                if (result){
                    req.flash('error', 'Email-Id already exist')
                    res.redirect('/register')
                }
                else {
                    User.findOne({
                        $and: [
                            { company: new_user.company },
                            { isCompanyOwner: true }
                        ]
                    }, function (err, companyOwner) {
                        //Procedure to create a new normal user

                        if (err) return console.log(err)
                        if (companyOwner) {
                            console.log("//Creating normal user")
                            new_user.group.push(companyOwner.group[0])
                            User.create(new_user, (err, added_user) => {


                                Group.findByIdAndUpdate(added_user.group[0], { $push: { user: new_user._id } }, (err, updated_group) => {
                                    if (err) throw err;
                                    console.log('Updated group:' + updated_group);

                                })
                            })

                        }
                        else {
                            console.log("Creating company owner")
                            //create new group
                            Group.create({
                                group_name: 'AllContacts',
                                template_id: default_template_id,
                                user: [new_user_id],
                                admin_id: new_user_id
                            }, (err, companyGroup) => {
                                if (err) throw err;
                                new_user.isCompanyOwner = true;
                                new_user.group.push(companyGroup._id)
                                User.create(new_user);
                            });
                            Group.create()
                        }
                        res.render('reg_login',{
                            email: req.body.email,
                            password: req.body.password
                        });
                    })
                }
            })
        }
    })





})

app.get('/:user_id/resend_otp', function(req, res){
    var token = otplib.totp.generate(secret)

    User.findByIdAndUpdate(req.params.user_id, {otp: token}, (err, user_data)=>{
        var nodemailer = require('nodemailer');
        var transport = nodemailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
                user: "145aa399c68c76",
                pass: "86a80afd21a1b9"
            }
        });
        var mailoptions={
            from:'superadmim@nimap.com',
            to : user_data.email,
            subject: 'Activate your account',
            text: `Your NEW OTP is ${token}`
        }

        transport.sendMail(mailoptions,(err,info)=>{
            console.log("new OTP Sent")
        })
    })
    res.redirect('/login/verifyEmail')
})

module.exports = app;
