const express = require('express');
const router = express.Router();
var Template = require('../models/template-model')
var Group = require('../models/group-model')
var User = require('../models/user-model')

var default_template_id
router.get('/', (req, res) => {
    var data = {
        name: "default Group",
        template_id: default_template_id,

    }

    Group.create(data, (err, group) => {
        var new_user = {
            username: 'req.body.username',
            email: 'req.body.email',
            password: 'hash',
            company: 'req.body.company',
            isCompanyOwner: true,
            isSuperAdmin: false,
            group: [group._id]
        }
        User.create(new_user)
    })
})
module.exports = router