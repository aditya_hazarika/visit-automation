var express = require('express')
var app = express()

var publicDir = require('path').join(__dirname,'../public');
app.use(express.static(publicDir))

app.get('/',function(req,res){
    res.render('index')
})

app.get('/register', function(req,res){
    res.render('register')
})

module.exports =app