var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session');
var nodemailer = require('nodemailer');
var authenticationController = require('./controller/authentication');
var methodOverride = require('method-override')
var flash = require('express-flash')
var passport = require('passport')
var passportSetup = require('./config/passport-setup')
var convertor = require('@iarna/rtf-to-html')
var mail = require('./controller/mail');
// var keys = require('./config/keys')

var company_owner = require('./routes/company_owner')
var user = require('./routes/user')
var super_admin = require('./routes/super_admin')
//var home = require('./routes/home')
//var local = require('./routes/local')

var LocalStrategy = require('passport-local')
// var passportLocalMongoose = require('passport-local-mongoose')
var keys = require('./config/keys')

//   var company_owner = require('./routes/company_owner')
//   var dashboard = require('./routes/dashboard')
var login = require('./routes/login')
var register = require('./routes/register')
var super_admin = require('./routes/super_admin')
var user = require('./routes/user')
var change_password = require('./routes/change_password')

var port = process.env.PORT || 8080;
mongoose.connect('mongodb://application:pranav555@ds155164.mlab.com:55164/node-visit-automation', function () {
    console.log('mongodb connected')
})

var app = express();

app.set('view engine', 'ejs')

app.use(cookieSession({
    maxAge: 24 * 60 * 60 * 1000, //1 Day
    keys: [keys.session.cookieKey]
}));

app.use(flash());

app.use(passport.initialize());
app.use(passport.session())

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        var method = req.body._method
        delete req.body._method
        return method
    }
}))
// app.use('/test',test)
// app.use('/compown', company_owner)
// app.use('/local', local)

// app.use('/superadmin', super_admin)
// app.use('/user', user)
// app.use('/',home)

// app.get('/secret', authenticationController.roleCheck("compown"), (req, res) => {
// res.send("You are logged in correctly")
// })
// app.get('/', function(req,res){
//     res.render('index',{
//         title: 'VISIT AUTOMATION', user: req.user
//     })
// })


// app.use('/dashboard', dashboard)

//Mobile api routes
const api = require('./routes/mobile/api')
app.use('/api', api)

//Web Panel Routes
app.use('/login', login)
app.use('/register', register)
app.use('/change_password', change_password)

app.use('/superadmin', authenticationController.roleCheck('superAdmin'), super_admin)
app.use('/user', authenticationController.roleCheck('usr'), user)
app.use('/compown', authenticationController.roleCheck('compown'), company_owner)

app.get('/', function(req, res){
    res.redirect('/login')
} )

app.get('/mail', (req, res) => {
    mail.sendMail(req.user,"prnvnachnekar@gmail.com","askjdbaksjb",'<b>bold</b>')
})
app.listen(port, function () {
    console.log('listening');
})